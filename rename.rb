require 'rest-client'
require 'optparse'
require 'colorize'

options = {}
o = OptionParser.new do |opts|
        opts.banner = "Usage: redirector_test.rb [options]"

        opts.on('-h', '--host HOST', 'The base url to the DiscDB service (http://discdb.bherville.com)') do |v| 
            options[:host] = v
        end

        opts.on('-k', '--key KEY', 'Your API key') do |v| 
            options[:key] = v
        end

        opts.on('-p', '--path PATH', 'The base path to the episodes to rename') do |v| 
            options[:path] = v
        end

        opts.on('-t', '--test', 'Run script in test mode, this will NOT rename files but will perform all other operations') do |v| 
            options[:test] = v
        end

        opts.on("--help", "Show this message") do
                puts opts
                exit
        end

end

o.parse!

unless options[:host] && options[:key] && options[:path]
        o.parse! ['--help']
end


API_HOST    = "#{options[:host]}/api"
DASH_COUNT  = 50

def get_disc(disc_label, options)	
    # Get Discs
	json = RestClient.get "#{API_HOST}/discs", {:Authorization => "Token token=#{options[:key]}", :params => {:disc_label => disc_label}}
	
	unless json
		puts "No discs found for #{disc_label}"
		return
	end
	
    discs = JSON.parse(json)
	
	if discs.count > 1
		# Prompt for input
		puts "Multiple Discs Found, please choose which disc to use."
		
		discs.each_with_index do |disc, index|
			puts "#{index + 1}. #{disc['tv_show']} - Season #{disc['season']}"
		end
		
		puts "Enter Disc #: "
		index = ($stdin.gets.chomp.to_i)-1
		
		disc = discs[index]
	else
		disc = discs[0]
	end
end

def get_episode(disc, order)
    ((disc['episode_discs'].select { |episode_disc| episode_disc['disc_order'] == order }).map { |ed| ed['episode'] }).uniq
end

def get_episode_file_name(episodes, episode_file_name)
    episode_substr = ''

    episodes.each do |episode|
        episode_substr = "#{episode_substr}e#{episode['number'].to_s.rjust(2, '0')}"
    end

    episode = episodes.first
    episode_name = episodes.count > 1 ? episode['name'].gsub(/(\s\([0-9]+\))/,'') : episode['name']

	"#{episode['tv_show']} - s#{episode['season'].to_s.rjust(2, '0')}#{episode_substr} - #{episode_name}#{File.extname(episode_file_name)}"
end

def display_header(options)
    title   = 'DiscDB Renamer'
    center  = (DASH_COUNT - title.length)/2

    puts '-'*DASH_COUNT
    puts "#{"\s"*center}#{title}#{"\s"*center}"
    puts '-'*DASH_COUNT
    puts "Host:\t\t#{options[:host]}"
    puts "API Key:\t#{options[:key]}"
    puts "Path:\t\t#{options[:path]}"
    puts "Test Mode:\t#{options[:test] ? 'Yes'.green : 'No'.red}"
    puts '-'*DASH_COUNT
    puts ''
    puts ''
    puts ''
end

display_header options

Dir.foreach(options[:path]) do |disc|
    next if disc == '.' or disc == '..'
    center  = (DASH_COUNT - disc.length)/2
    puts "#{"\s"*center}#{disc}#{"\s"*center}"
    puts '-'*DASH_COUNT
	
	api_disc = get_disc(disc, options)
    unless api_disc
        puts "Disc with disc label \"#{disc}\" not found!"
        next
    end
	
    disc_order = 1
    Dir.foreach(File.join(options[:path], disc)) do |episode|
        next if episode == '.' or episode == '..'
        eps = get_episode(api_disc, disc_order)
		next unless eps
		
		new_name = get_episode_file_name(eps, episode).gsub(/[\x00\/\\:\*\?\"<>\|]/, '')        
		
        puts "Original file name:      #{episode}"
        puts "New Name:                #{new_name}"
		
		File.rename(File.join(options[:path], disc, episode), File.join(options[:path], disc, new_name)) unless options[:test]
        
        disc_order = disc_order+1
    end
    puts ''
    puts ''
    puts ''
end
